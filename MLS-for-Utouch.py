#!/usr/bin/env python3


#Copyright (C) 2019 Ernesst <ernesst@posteo.net>
#This program is free software; you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation; version 3.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

# 0.1 intial release
# 0.2 update UI

import time
import subprocess
import re
import json
import time
import sys
import json
import requests
import os
import multiprocessing

release = "0.2"

i = 0

def read_gps():
	global latitude
	global longitude
	global accuracy
	latitude = str("")
	longitude = str("")
	accuracy = str("")
	cmd = ['sudo test_gps']
	p = subprocess.Popen(cmd, stdout = subprocess.PIPE,
	        stderr = subprocess.STDOUT, shell = True)
	for line in p.stdout:
		line = line.decode('utf-8')
		if re.search("^latitude", line):
				latitude = line.split()
				latitude = latitude[1]

		if re.search("^longtide", line): #bug in test_gps
				longitude = line.split()
				longitude = longitude[1]

		if re.search("^accuracy", line): #bug in test_gps
				accuracy = line.split()
				accuracy= accuracy[1]
		if accuracy != "":
			#print(line + "=> accuracy : "  + accuracy)
			#print(line + "=> longitude : "  + longitude)
			#print(line + "=> latitude : "  + latitude)
			p.kill()
			return accuracy,longitude,latitude

def read_wifi(lat, lon, acc):
	cmdstr = ['sudo iwlist wlan0 scanning']
	proc = subprocess.Popen(cmdstr, shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
	stdout, stderr = proc.communicate()
	stdout = stdout.decode('utf-8')
	Scan_list = []
	Scan_list = re.split("\n          Cell ", stdout)
	Scan_list.pop(0)
	wifi_host = []
	json_item = {}
	json_item = {"timestamp" : int(round(time.time() * 1000)),"position":{}, "wifiAccessPoints" : []}
	position = {"latitude": lat, "longitude" :lon, "accuracy": acc}
	json_item["position"] = position
	age = ''
	radio_type = ''
	Frequency = ''
	channe = ''
	mac = ''
	for router in Scan_list:
			wifi_host = router.split("\n          ")
			#print("****************************")
			for row in wifi_host:
				row = row.strip()
				#if re.search("^Frequency:", row):
					#Frequency = row.split()
					#Frequency = Frequency[0].split(":")
					#Frequency = Frequency[1]
					#Frequency = Frequency
					#print(Frequency)
					#print(row + " => " + Frequency)
					#item[row.frequency] = Frequency
				if re.search("^Channel", row):
					channel = row.split(":")
					channel = channel[1]
					#print(row+ " => " + channel )
				if re.search("- Address:", row):
					mac = row.split()
					mac = mac[3]
					#print(row+ " => " + mac)
				if re.search("^IE: IEEE", row):
					radio_type = row.split()
					radio_type = radio_type[2]
					radio_type = radio_type.split("/")
					#print(radio_type)
					radio_type = radio_type[0]
					#print(row + " => " + radio_type)
				if re.search("^Extra: Last beacon:", row):
					age = row.split()
					age = age[3]
					age = age[:-2]
					#print(row + " => " + age)
			json_item['wifiAccessPoints'].append({"frequency":Frequency, "channel":channel, "macAddress": mac, "age": age, "radioType": radio_type})
	print("Wifi networks discovered : " + str(len(json_item['wifiAccessPoints'])))
	data = {}
	data["items"] = []
	try:
		with open('data.txt', "r") as masterjf:
			try:
				data = json.load(masterjf)
				data["items"].append(json_item)
			except:
				data["items"].append(json_item)

	except:
		data["items"].append(json_item)

	with open('data.txt', 'w+') as outfile:
		json.dump(data, outfile)

try:
	os.system('clear')
	print("\n***************************")
	print("**  UTouch-MLS tool " + str(release)+"  **")
	print("***************************")
	print("\nWelcome to the UTouch-MLS tool to enhance the Mozilla Location Service database for your own convience.")
	print("The scan will start automatically, scan result will be recorded in the data.txt file next to this tool.")
	print("To close the application simply select Ctrl+c virtual key, the data will be automatically uploaded to https://location.services.mozilla.com")
	print("Be sure your phone is connected to internet for the upload.")
	print("Enjoy !")
	print("\n")
	while True:
		read_gps()
		os.system('clear')
		print("\n***************************")
		print("**  UTouch-MLS tool " + str(release)+"  **")
		print("***************************")
		#print("\nWelcome to the UTouch-MLS tool to enhance the Mozilla Location Service database for your own convience.")
		#print("The scan will start automatically, scan result will be recorder in the data.txt file next to this file.")
		print("To close the application simply select Ctrl+c virtual key, the data will be automatically uploaded to https://location.services.mozilla.com")
		print("Be sure your phone is connected to internet for the upload.")
		#print("Enjoy !")
		print("\n")
		# read_gps()
		print("Scan Number : " + str(i))
		print("latitude : " + latitude + " longitude : " + longitude)
		read_wifi(latitude,longitude,accuracy)
		time.sleep(3)
		i = i + 1

except KeyboardInterrupt:
	print("\n \nUploading the information to https://location.services.mozilla.com, please wait.")
	url = 'https://location.services.mozilla.com/v2/geosubmit'
	with open('data.txt') as json_file:
		payload = json.load(json_file)
	headers = {'content-type': 'application/json'}
	try:
		response = requests.post(url, data=json.dumps(payload), headers=headers)
		if response.status_code == 200:
				print("\n***************************")
				print("Data uploaded successfully.")
				print("***************************")
				os.remove("data.txt")
		else:
				print("\nSorry an issue happen during the upload, the collect is not lost see data.txt.")
		time.sleep(2)
		print("Thanks for the contribution. \n")

	except:
		print("\n !!!Sorry an issue happen during the upload, the collect is not lost see data.txt")

	sys.exit()
