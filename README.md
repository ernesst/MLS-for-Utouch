# MLS-for-Utouch

UTouch-MLS is a python tool which need to be run from an ubuntu touch terminal, if the terminal is set to run in background UTouch-MLs should run in background too.
This tool is able to scan wifi around your device and capture the GPS location to populate the Mozilla Location Service database.

The python tool is a wrapper using :
- for the GPS : test_gps Location
- for the Wifi scan : sudo iwlist wlan0 scanning
- Upload data address : https://location.services.mozilla.com

#### Installation :
Copy the .py file where you want.
allow to execute it by : chmod a+x MLS-for-Utouch.py

#### How to use it :
Open a terminal, go where the file is store and launch it by : sudo ./MLS-for-Utouch.py

### Note :
So far this tool has been only test on a Nexus 5 - Hammerhead device.

bug report are welcome, any integration as a qml app too.

Ernesst
